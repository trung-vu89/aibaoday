var express         = require('express');
var fs              = require('fs');
var mongoose        = require('mongoose');
var passport        = require('passport');
var expressSession  = require('express-session');
var flash           = require('connect-flash');
var bodyParser      = require('body-parser');
var cookieParser    = require('cookie-parser');

var app = express();

app.use(expressSession({
  secret: '5102yadoabia',
  resave: false,
  saveUninitialized: false
})); // aibaoday2015
app.use(passport.initialize());
app.use(passport.session());

// MongoDB connection via mongoose
var dbOptions = {
  server: {
    socketOptions: { keepAlive: 1 }
  }
};
var dbConfig = require('./configs/db.js');
mongoose.connect(dbConfig.connectionString(app.get('env')), dbOptions);

// Server port
app.set('port', process.env.PORT || 8080);

// Handlebars view engine
var handlebars = require('express-handlebars').create({
  defaultLayout: 'main',
  helpers: {
    loadFirstPageArticleList: function(dataList) {},
  }
});
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

// 'views' folder
app.set('views', __dirname + '/views');

// All static files in 'public' folder
app.use(express.static(__dirname + '/public'));

// Flash message configs
app.use(flash());

// Cookie Parser initialization
app.use(cookieParser());

// Body Parser initialization
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Passport authentication initialization
var authentication = require('./controllers/authentication/init');
authentication(passport);

// dynamically include routes (Controller)
var indexRoute = require('./controllers/index');
indexRoute.registerRoutes(app);

var userRoute = require('./controllers/user');
userRoute.registerRoutes(app, passport);

var searchRoute = require('./controllers/search');
searchRoute.registerRoutes(app);

var adminRoute = require('./controllers/admin');
adminRoute.registerRoutes(app);

var errorRoute = require('./controllers/error');
errorRoute.registerRoutes(app);

var server = app.listen(app.get('port'), function() {
  var host = server.address().address;
  var port = server.address().port;
  console.log("AiBaoDay web application has been started at http://%s:%s", host, port);
});
server.timeout = 10000; // default response timeout 10s
