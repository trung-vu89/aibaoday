var mongoose = require('mongoose');

var articleSchema = mongoose.Schema({
	id: String,
	title: String,
	titleUrl: String,
	description: String,
	category: String,
	thumbnailImage: String,
	author: String,
	content: String,
	tags: [String],
	views: Number,
	published: Boolean,
	createdAt: { type: Date, default: Date.now },
	updatedAt: { type: Date, default: Date.now }
});

// Get all Article documents
articleSchema.methods.getAll = function(callback) {
	return this.model('Article').find().sort({'createdAt' : 'descending'}).exec(callback);
}

// Get all active Article documents
articleSchema.methods.getActive = function(callback) {
	return this.model('Article').where({ 'published' : 'true' }).sort({ 'createdAt' : 'descending' }).exec(callback);
}

// Get one Article document by Id
articleSchema.methods.getById = function(id, callback) {
	return this.model('Article').findById(id, callback);
}

// Get one Article document by titleUrl
articleSchema.methods.getByTitleUrl = function(inputTitleUrl, callback) {
	return this.model('Article').find({ titleUrl : inputTitleUrl }, callback);
}

// Get latest Article document except the one with Id of parameter
articleSchema.methods.getLatest = function(exceptId, limit, callback) {
	return this.model('Article').find({ _id : { $ne : exceptId } }).sort({ 'createdAt' : 'descending' }).limit(limit).exec(callback);
}

// Get top view Article documents today
articleSchema.methods.getTopViewToday = function(exceptIds, limit, callback) {
 	return this.model('Article').find({ _id : { $not : { $in : exceptIds } } }).sort({ updatedAt : -1, views : -1 }).limit(limit).exec(callback);
}

// Remove an Article document found by Id
articleSchema.methods.removeById = function(id, callback) {
	return this.model('Article').findByIdAndRemove(id, callback);
}

// Count total number of Article documents
articleSchema.methods.countAll = function(callback) {
	return this.model('Article').count({}, callback);
}

// Search all Article documents contains search keyword
articleSchema.methods.search = function(keyword, callback) {
	return this.model('Article').find({ $or: [
    { "title" : {$regex: new RegExp('^' + keyword.toLowerCase(), 'i')} },
    { "content" : {$regex : ".*" + keyword + ".*"} },
    { "description" : {$regex : ".*" + keyword + ".*"} },
    { "category" : {$regex : ".*" + keyword + ".*"} },
    { "tags" : {$regex : ".*" + keyword + ".*"} }
  ] }, callback);
}

// Format Article title to the url path format
articleSchema.methods.formatTextForUrl = function(inputArticleTitle) {
	var outputArticleName = inputArticleTitle.replace(/-/g,"");
	outputArticleName = outputArticleName.replace(/\//g, "-");
  outputArticleName = outputArticleName.replace(/\s\s+/g," ");
  outputArticleName = outputArticleName.replace(/ /g,"-");
  outputArticleName = outputArticleName.toLowerCase();
  outputArticleName = outputArticleName.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
	outputArticleName = outputArticleName.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
	outputArticleName = outputArticleName.replace(/ì|í|ị|ỉ|ĩ/g,"i");
	outputArticleName = outputArticleName.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
	outputArticleName = outputArticleName.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
	outputArticleName = outputArticleName.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
	outputArticleName = outputArticleName.replace(/đ/g,"d");
  outputArticleName = outputArticleName.replace(/[^a-zA-Z\d\s-]/g, "");
  return outputArticleName;
}

var Article = mongoose.model('Article', articleSchema);
module.exports = Article;
