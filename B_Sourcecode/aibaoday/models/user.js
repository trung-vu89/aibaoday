var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
  id: String,
  username: String,
  password: String,
  email: String,
  role: String,
  createdAt: { type: Date, default: Date.now }
});

// Get all User documents
userSchema.methods.getAll = function(callback) {
	return this.model('User').find({}, callback);
}

// Get one User document by Id
userSchema.methods.getById = function(id, callback) {
	return this.model('User').findById(id, callback);
}

// Get one User document by name
userSchema.methods.getByRole = function(role, callback) {
  return this.model('User').findOne({ 'role' : role }, callback);
}

// Count total number of User documents
userSchema.methods.countAll = function(callback) {
	return this.model('User').count({}, callback);
}

var User = mongoose.model('User', userSchema);
module.exports = User;
