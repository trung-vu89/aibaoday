var mongoose = require('mongoose');

var categorySchema = mongoose.Schema({
  id: String,
  name: String,
  nameFormat: String,
  description: String,
  viewOrder: Number,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

// Get all Category documents sorting by time
categorySchema.methods.getAll = function(callback) {
	return this.model('Category').find().sort({'updatedAt' : 'descending'}).exec(callback);
}

// Get all Category documents sorting by view order
categorySchema.methods.getAllByViewOrder = function(callback) {
  return this.model('Category').find().sort({ 'viewOrder' : 'ascending' }).exec(callback);
}

// Get one Category document by Id
categorySchema.methods.getById = function(id, callback) {
	return this.model('Category').findById(id, callback);
}

// Get one Category document by name
categorySchema.methods.getByName = function(name, callback) {
  return this.model('Category').findOne({ 'name' : name }, callback);
}

// Get one Category document by nameFormat
categorySchema.methods.getByNameFormat = function(nameFormat, callback) {
  return this.model('Category').findOne({ 'nameFormat' : nameFormat }, callback);
}

// Remove an Category document found by Id
categorySchema.methods.removeById = function(id, callback) {
	return this.model('Category').findByIdAndRemove(id, callback);
}

// Count total number of Category documents
categorySchema.methods.countAll = function(callback) {
	return this.model('Category').count({}, callback);
}

// Format Article title to the url path format
categorySchema.methods.formatName = function(inputCategoryName) {
	var outputCategoryName = inputCategoryName.replace(/-/g,"");
	outputCategoryName = outputCategoryName.replace(/\//g, "-");
  outputCategoryName = outputCategoryName.replace(/\s\s+/g," ");
  outputCategoryName = outputCategoryName.replace(/ /g,"-");
  outputCategoryName = outputCategoryName.toLowerCase();
  outputCategoryName = outputCategoryName.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
	outputCategoryName = outputCategoryName.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
	outputCategoryName = outputCategoryName.replace(/ì|í|ị|ỉ|ĩ/g,"i");
	outputCategoryName = outputCategoryName.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
	outputCategoryName = outputCategoryName.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
	outputCategoryName = outputCategoryName.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
	outputCategoryName = outputCategoryName.replace(/đ/g,"d");
  outputCategoryName = outputCategoryName.replace(/[^a-zA-Z\d\s-]/g, "");
  return outputCategoryName;
}

var Category = mongoose.model('Category', categorySchema);
module.exports = Category;
