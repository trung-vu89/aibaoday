var mongoose = require('mongoose');

var roleSchema = mongoose.Schema({
  id: String,
  name: String,
  description: String,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

// Get all Role documents
roleSchema.methods.getAll = function(callback) {
	return this.model('Role').find({}, callback);
}

// Get one Role document by Id
roleSchema.methods.getById = function(id, callback) {
	return this.model('Role').findById(id, callback);
}

// Get one Role document by name
roleSchema.methods.getByName = function(name, callback) {
  return this.model('Role').findOne({ 'name' : name }, callback);
}

// Count total number of Role documents
roleSchema.methods.countAll = function(callback) {
	return this.model('Role').count({}, callback);
}

// Remove an Role document found by Id
roleSchema.methods.removeById = function(id, callback) {
	return this.model('Role').findByIdAndRemove(id, callback);
}

var Role = mongoose.model('Role', roleSchema);
module.exports = Role;
