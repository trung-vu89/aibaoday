var Category = require('../models/category');
var Article = require('../models/article');
var ArticleHelper = require('../helpers/article');
var Handlebars = require('handlebars');

var articleModel = new Article();
var categoryModel = new Category();
var articleHelper = new ArticleHelper();

var categoriesList = undefined;

exports.registerRoutes = function(app) {
  app.get('/search/:keyword', getAllCategories, function (req, res) {
    searchContent(req, res);
  });
};

// Search article
function searchContent(req, res) {
  var searchKeyword = req.params.keyword;
  articleModel.search(searchKeyword, function(searchError, docs) {
    if (searchError) {
      console.log("Cannot find Article with name: " + req.params.articleName);
      res.status(404);
      res.render('errors/404');
    } else {
      var articlesList = [];
      for (var i = 0; i < docs.length; i++) {
        var inputArticle = docs[i];
        articlesList.push({
          id: inputArticle._id,
          title : inputArticle.title,
          titleUrl: inputArticle.titleUrl,
          description : inputArticle.description,
          category : inputArticle.category,
          categoryUrl : inputArticle.formatTextForUrl(inputArticle.category),
          thumbnailImage : inputArticle.thumbnailImage,
          author : inputArticle.author,
          content : inputArticle.content,
          tags : inputArticle.tags,
          views : inputArticle.views,
          updatedAt : formatDateTime(inputArticle.updatedAt),
        });
      }
      res.render('search/result', {
        categories : categoriesList,
        keyword : searchKeyword,
        articleCount: articlesList.length,
        articles: articlesList,
        metaTitle : "Ai Báo Đây!",
        metaDescription : "Báo đây, ai báo đây. Tin nóng, tin nhanh, hotgirl, showbiz, chính trị, quốc tế... Nóng hổi vừa thổi vừa coi!",
        metaImage : "",
        metaUrl : "http://www.aibaoday.com",
        helpers : {
          loadFirstPageArticleList: function(dataList) {
            return new Handlebars.SafeString(articleHelper.generateListTags(dataList));
          }
        }
      });
    }
  });
}

// Get all categories
function getAllCategories(req, res, next) {
  categoryModel.getAllByViewOrder(function(err, docs) {
    if (err) {
      categoriesList = [];
    } else {
      categoriesList = docs;
    }
    next();
  });
}

function formatDateTime(datetime) {
  var hours = datetime.getHours();
  var minutes = datetime.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return datetime.getDate() + "/" + parseInt(datetime.getMonth()+1) + "/" + datetime.getFullYear() + "  " + strTime;
}
