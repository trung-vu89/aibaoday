var User = require('../models/user');
var Category = require('../models/category');
var Role = require('../models/role');
var Article = require('../models/article');
var Request = require('request');

var ARTICLES_PER_PAGE = 50;

var userModel = new User();
var categoryModel = new Category();
var roleModel = new Role();
var articleModel = new Article();

var isAuthenticated = function (req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated())
		return next();
	// if the user is not authenticated then redirect him to the login page
	res.redirect('/users/sign_in');
}

// Counting
var totalUsers = undefined;
var totalCategories = undefined;
var totalRoles = undefined;
var totalArticles = undefined;

// List
var categoriesList = undefined;
var rolesList = undefined;
var usersList = undefined;
var articlesList = undefined;

exports.registerRoutes = function(app) {
  // Admin main page
  app.get('/admin/index', isAuthenticated, getTotalUsers, getTotalCategories, getTotalRoles, getTotalArticles, function(req, res) {
    res.render('admins/index', {
      layout: 'admin',
      username: req.user.username,
      userCount: totalUsers,
      categoryCount: totalCategories,
      roleCount: totalRoles,
      articleCount: totalArticles
    });
  });

  // Category management
  // Category main page
  app.get('/admin/category', isAuthenticated, getAllCategories, function(req, res) {
    res.render('admins/categories', { layout: 'admin', csrf: 'categories_management', username: req.user.username, categories: categoriesList });
  });
  // Category create new
  app.post('/admin/category/new', isAuthenticated, function(req, res) {
		app.locals.categoryLastUpdatedAt = getCurrentDateTime();
    addNewCategory(req, res);
  });
  // Category edit/update
  app.post('/admin/category/edit', isAuthenticated, function(req, res) {
		app.locals.categoryLastUpdatedAt = getCurrentDateTime();
    editCategory(req, res);
  });
	// Category remove
  app.post('/admin/category/delete', isAuthenticated, function(req, res) {
		app.locals.categoryLastUpdatedAt = getCurrentDateTime();
    deleteCategory(req, res);
  });

	// Role management
	// Role main page
	app.get('/admin/role', isAuthenticated, getAllRoles, function(req, res) {
		res.render('admins/roles', { layout: 'admin', csrf: 'roles_management', username: req.user.username, roles: rolesList });
	});
	// Role create new
  app.post('/admin/role/new', isAuthenticated, function(req, res) {
    addNewRole(req, res);
  });
	// Role edit/update
  app.post('/admin/role/edit', isAuthenticated, function(req, res) {
    editRole(req, res);
  });
	// Role remove
  app.post('/admin/role/delete', isAuthenticated, function(req, res) {
    deleteRole(req, res);
  });

	// User management
	// User main page
	app.get('/admin/user', isAuthenticated, getAllUsers, getAllRoles, function(req, res) {
		res.render('admins/users', { layout: 'admin', csrf: 'users_management', username: req.user.username, users: usersList, userRoles: rolesList });
	});
	// User Role update
  app.post('/admin/user/role', isAuthenticated, function(req, res) {
    updateUserRole(req, res);
  });

  // Article management
  // Article main page
  app.get('/admin/article', isAuthenticated, getAllArticles, function(req, res) {
		var maxPage = Math.ceil(articlesList.length / ARTICLES_PER_PAGE);
		var currentPageNumber = parseInt(req.query.page);
		if (!currentPageNumber) {
			currentPageNumber = 0;
		} else {
			if (currentPageNumber >= maxPage) {
				currentPageNumber = maxPage;
			}
			currentPageNumber = currentPageNumber - 1;
		}
		var fromIndex = currentPageNumber * ARTICLES_PER_PAGE;
		var toIndex = (currentPageNumber + 1) * ARTICLES_PER_PAGE;
		var currentPageArticles = [];
		for (var i = fromIndex; i < toIndex; i++) {
			if (i < articlesList.length)
			{
				currentPageArticles.push(articlesList[i]);
			}
		}
    res.render('admins/articles', { layout: 'admin', csrf: 'articles_management', username: req.user.username, articles: currentPageArticles, maxPage : maxPage });
  });

  // Article create new (view)
  app.get('/admin/article/new', isAuthenticated, getAllCategories, function(req, res) {
    res.render('admins/article_edit', { layout: 'admin', csrf: 'article_new', username: req.user.username, categories: categoriesList });
  });

  // Article create new (save data to db)
	app.post('/admin/article/new', isAuthenticated, function(req, res) {
		saveArticle(req, res, function(success) {
			if (success === true) {
				app.locals.articleLastUpdatedAt = getCurrentDateTime();
				res.redirect('/admin/article');
			} else {
				console.log("Error in database!");
				res.status(500);
		    res.render('errors/500');
			}
		});
	});

	// Update Publish Status of Article
	app.post('/admin/article/update', isAuthenticated, function(req, res) {
		updateArticleStatus(req, res, function(result) {
			app.locals.articleLastUpdatedAt = getCurrentDateTime();
			res.send({ success: result });
		});
	});

	// Show Article Edit view
	app.get('/admin/article/edit/:id', isAuthenticated, getAllCategories, function(req, res) {
		var articleId = req.params.id;
		articleModel.getById(articleId, function(err, doc) {
			if (err) {
				console.log("Cannot find article with id: " + articleId);
				res.status(500);
		    res.render('errors/500');
			} else {
				res.render('admins/article_edit', { layout: 'admin', csrf: 'article_edit', username: req.user.username, article: doc, categories: categoriesList });
			}
		});
	});

  // Update Article
	app.post('/admin/article/edit/:id', isAuthenticated, function(req, res) {
		saveArticle(req, res, function(success) {
			if (success) {
				app.locals.articleLastUpdatedAt = getCurrentDateTime();
				res.redirect('/admin/article');
			} else {
				res.status(500);
		    res.render('errors/500');
			}
		});
	});

	// Delete Article
	app.post('/admin/article/delete', isAuthenticated, function(req, res) {
		deleteArticle(req, res, function(result) {
			app.locals.articleLastUpdatedAt = getCurrentDateTime();
			res.send({ success: result });
		});
	});

	// Generate Article title url
	app.get('/admin/article/generateTitleUrl', isAuthenticated, function(req, res) {
		articleModel.getAll(function(err, docs) {
	    if (err) {
	      console.log("No Articles data found...");
	    } else {
	      for (var i = 0; i < docs.length; i++) {
	      	docs[i].titleUrl = docs[i].formatTextForUrl(docs[i].title);
					docs[i].save(function(error) {
						if (error) {
							console.log("Save Article failed!");
						}
					});
	      }
	    }
	  });
	});

// Fetch article data from resource uri
	app.get('/admin/tool/fetch', isAuthenticated, function(req, res) {
		var sourceUrl = req.query.sourceUrl;
		Request(sourceUrl, function(error, response, body) {
			if (!error && response.statusCode == 200) {
				res.send({ success : body });
			} else {
				res.send({ error : error });
			}
		});
	});
};

// --- User - begin ---
function getTotalUsers(req, res, next) {
  userModel.countAll(function(err, count) {
    if (err) {
      totalUsers = 0;
    } else {
      totalUsers = count;
    }
    next();
  });
}

function getAllUsers(req, res, next) {
	userModel.getAll(function(err, docs) {
		if (err) {
			usersList = [];
		} else {
			usersList = docs;
		}
		next();
	});
}

function updateUserRole(req, res) {
	if (!req.body.userId)
    return res.send({ success: false });
	userModel.getById(req.body.userId, function (err, user) {
    if (err) {
      console.log('Error in database: ' + err);
      return res.send({ success: false });
    }
    if (!user) {
      console.log('User does not exist with id: ' + req.body.userId);
      return res.send({ success: false });
    } else {
      user.role = req.body.roleName;
      user.updatedAt = getCurrentDateTime();
      user.save(function(err) {
        if (err) {
          console.log('Error in saving User: ' + err);
          return res.send({ success: false });
        }
        console.log('User Role was updated!');
        if (req.xhr || req.accepts('json,html') === 'json') {
          res.send({ success: true });
        } else {
          res.redirect('/admin/user', req.flash('message', 'Success'));
        }
      });
    }
  });
}
// --- User - end ---

// --- Role - begin ---
function getTotalRoles(req, res, next) {
  roleModel.countAll(function(err, count) {
    if (err) {
      totalRoles = 0;
    } else {
      totalRoles = count;
    }
    next();
  });
}

function getAllRoles(req, res, next) {
	roleModel.getAll(function(err, docs) {
		if (err) {
			rolesList = [];
		} else {
			rolesList = docs;
		}
		next();
	});
}

function addNewRole(req, res) {
  if (!req.body.roleName || !req.body.roleDescription)
    return res.send({ success: false });
  roleModel.getByName(req.body.roleName, function(err, role) {
    if (err) {
      console.log('Error in database: ' + err);
      return res.send({ success: false });
    }
    if (role) {
      console.log('Role already exists with name: ' + req.body.roleName);
      return res.send({ success: false });
    } else {
      var newRole = new Role();
      newRole.name = req.body.roleName;
      newRole.description = req.body.roleDescription;
      newRole.createdAt = getCurrentDateTime();
      newRole.updatedAt = getCurrentDateTime();
      newRole.save(function(err) {
        if (err) {
          console.log('Error in saving Role: ' + err);
          return res.send({ success: false });
        }
        console.log('New Role was added!');
        if (req.xhr || req.accepts('json,html') === 'json') {
          res.send({ success: true });
        } else {
          res.redirect('/admin/role', req.flash('message', 'Success'));
        }
      });
    }
  });
}

function editRole(req, res) {
  if (!req.body.roleId || !req.body.roleName || !req.body.roleDescription)
    return res.send({ success: false });
  roleModel.getById(req.body.roleId, function (err, role) {
    if (err) {
      console.log('Error in database: ' + err);
      return res.send({ success: false });
    }
    if (!role) {
      console.log('Role does not exist with name: ' + req.body.roleName);
      return res.send({ success: false });
    } else {
      role.name = req.body.roleName;
      role.description = req.body.roleDescription;
      role.updatedAt = getCurrentDateTime();
      role.save(function(err) {
        if (err) {
          console.log('Error in saving Role: ' + err);
          return res.send({ success: false });
        }
        console.log('Role was updated!');
        if (req.xhr || req.accepts('json,html') === 'json') {
          res.send({ success: true });
        } else {
          res.redirect('/admin/role', req.flash('message', 'Success'));
        }
      });
    }
  });
}

function deleteRole(req, res) {
  if (!req.body.roleId)
    return res.send({ success: false });
  roleModel.removeById(req.body.roleId, function(err, role) {
    if (err) {
      console.log('Error in deleting Role: ' + err);
      return res.send({ success: false });
    }
    console.log('Role ' + role.name + ' was deleted!');
    userModel.getByRole(role.name, function(error, usersByRole) {
      if (error) {
        console.log('Error in searching Uers by ' + role.name + ' role: ' + err);
        return res.send({ success: false });
      };
      if (usersByRole) {
        for (var i = usersByRole.length - 1; i >= 0; i--) {
          usersByRole[i].role = "";
          usersByRole[i].updatedAt = getCurrentDateTime();
          usersByRole[i].save();
        };
      };
      if (req.xhr || req.accepts('json,html') === 'json') {
        res.send({ success: true });
      } else {
        res.redirect('/admin/role');
      }
    });
  });
}
// --- Role - end ---

// --- Category - begin ---
function getTotalCategories(req, res, next) {
  categoryModel.countAll(function(err, count) {
    if (err) {
      totalCategories = 0;
    } else {
      totalCategories = count;
    }
    next();
  });
}

function getAllCategories(req, res, next) {
  categoryModel.getAllByViewOrder(function(err, docs) {
    if (err) {
      categoriesList = [];
    } else {
      categoriesList = docs;
    }
    next();
  });
}

function addNewCategory(req, res) {
  if (!req.body.categoryName || !req.body.categoryDescription || !req.body.categoryViewOrder)
    return res.send({ success: false });
  categoryModel.getByName(req.body.categoryName, function(err, category) {
    if (err) {
      console.log('Error in database: ' + err);
      return res.send({ success: false });
    }
    if (category) {
      console.log('Category already exists with name: ' + req.body.categoryName);
      return res.send({ success: false });
    } else {
      var newCategory = new Category();
      newCategory.name = req.body.categoryName;
			newCategory.nameFormat = newCategory.formatName(req.body.categoryName);
      newCategory.description = req.body.categoryDescription;
			newCategory.viewOrder = req.body.categoryViewOrder;
      newCategory.createdAt = getCurrentDateTime();
      newCategory.updatedAt = getCurrentDateTime();
      newCategory.save(function(err) {
        if (err) {
          console.log('Error in saving Category: ' + err);
          return res.send({ success: false });
        }
        console.log('New Category was added!');
        if (req.xhr || req.accepts('json,html') === 'json') {
          res.send({ success: true });
        } else {
          res.redirect('/admin/category', req.flash('message', 'Success'));
        }
      });
    }
  });
}

function editCategory(req, res) {
  if (!req.body.categoryId || !req.body.categoryName || !req.body.categoryDescription || !req.body.categoryViewOrder)
    return res.send({ success: false });
  categoryModel.getById(req.body.categoryId, function (err, category) {
    if (err) {
      console.log('Error in database: ' + err);
      return res.send({ success: false });
    }
    if (!category) {
      console.log('Category does not exist with name: ' + req.body.categoryName);
      return res.send({ success: false });
    } else {
      category.name = req.body.categoryName;
			category.nameFormat = category.formatName(req.body.categoryName);
      category.description = req.body.categoryDescription;
			category.viewOrder = req.body.categoryViewOrder;
      category.updatedAt = getCurrentDateTime();
      category.save(function(err) {
        if (err) {
          console.log('Error in saving Category: ' + err);
          return res.send({ success: false });
        }
        console.log('Category was updated!');
        if (req.xhr || req.accepts('json,html') === 'json') {
          res.send({ success: true });
        } else {
          res.redirect('/admin/category', req.flash('message', 'Success'));
        }
      });
    }
  });
}

function deleteCategory(req, res) {
  if (!req.body.categoryId)
    return res.send({ success: false });
  categoryModel.removeById(req.body.categoryId, function(err, category) {
    if (err) {
      console.log('Error in deleting Category: ' + err);
      return res.send({ success: false });
    }
    console.log('Category was deleted!');
    if (req.xhr || req.accepts('json,html') === 'json') {
      res.send({ success: true });
    } else {
      res.redirect('/admin/category');
    }
  });
}
// --- Category - end ---

// --- Article - start ---
function getTotalArticles(req, res, next) {
  articleModel.countAll(function(err, count) {
    if (err) {
      totalArticles = 0;
    } else {
      totalArticles = count;
    }
    next();
  });
}

function getAllArticles(req, res, next) {
  articleModel.getAll(function(err, docs) {
    if (err) {
      articlesList = [];
    } else {
      articlesList = docs;
    }
    next();
  });
}

function saveArticle(req, res, onFinished) {
	if (!req.body.title || !req.body.description || !req.body.thumbnailImage || !req.body.author || !req.body.content) {
		onFinished(false);
		return;
	}
	if (!req.body.articleId) { // create new
		var newArticle = new Article();
		var inputArticleName = req.body.title;
		// inputArticleName = inputArticleName.replace(/\//g, "-");
		newArticle.title = inputArticleName;
		newArticle.titleUrl = newArticle.formatTextForUrl(inputArticleName);
		newArticle.description = req.body.description;
		newArticle.category = req.body.category;
		newArticle.thumbnailImage = req.body.thumbnailImage;
		newArticle.author = req.body.author;
		newArticle.content = req.body.content;
		newArticle.tags = req.body.tags.split(",");
		newArticle.views = 0;
		newArticle.published = true;
		newArticle.createdAt = getCurrentDateTime();
		newArticle.updatedAt = getCurrentDateTime();
		newArticle.save(function(error) {
			if (error) {
				console.log("Cannot save Article into database");
				onFinished(false);
			} else {
				onFinished(true);
			}
		});
	} else {
		articleModel.getById(req.body.articleId, function(findErr, doc) {
			if (findErr) {
				console.log("Cannot find Article with id: " + req.body.articleId);
				onFinished(false);
			} else {
				var inputArticleName = req.body.title;
				// inputArticleName = inputArticleName.replace(/\//g, "-");
				doc.title = inputArticleName;
				doc.titleUrl = doc.formatTextForUrl(inputArticleName);
				doc.description = req.body.description;
				doc.category = req.body.category;
				doc.thumbnailImage = req.body.thumbnailImage;
				doc.author = req.body.author;
				doc.content = req.body.content;
				doc.tags = req.body.tags.split(",");
				doc.updatedAt = getCurrentDateTime();
				doc.save(function(err) {
					if (err) {
						console.log("Cannot save Article into database");
						onFinished(false);
					} else {
						onFinished(true);
					}
				});
			}
		});
	}
}

function updateArticleStatus(req, res, onFinished) {
	if (!req.body.articleId) {
		onFinished(false);
		return;
	}
	articleModel.getById(req.body.articleId, function(err, doc) {
		if (err) {
			console.log("Something wrong in database!");
			onFinished(false);
		} else {
			if (doc) {
				doc.published = req.body.published;
				doc.updatedAt = getCurrentDateTime();
				doc.save(function(err) {
					if (err) {
						console.log("Cannot save article document!");
						onFinished(false);
					} else {
						console.log("Article status was updated!");
						onFinished(true);
					}
				});
			} else {
				console.log("Cannot find article with id: " + req.body.articleId);
				onFinished(false);
			}
		}
	});
}

function deleteArticle(req, res, onFinished) {
	if (!req.body.articleId) {
		onFinished(false);
		return;
	}
	articleModel.removeById(req.body.articleId, function(err, article) {
		if (err) {
      console.log('Error in deleting Article: ' + err);
      onFinished(false);
    } else {
			console.log('Deleted Article: ' + article._id);
      onFinished(true);
    }
	});
}
// --- Article - end ---

function getCurrentDateTime() {
	var currentUTCTime = new Date();
	currentUTCTime.setMinutes(currentUTCTime.getMinutes() - 120); // convert to Vietnam local time
	return currentUTCTime;
}
