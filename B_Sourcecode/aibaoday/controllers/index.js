var Category = require('../models/category');
var Article = require('../models/article');
var ArticleHelper = require('../helpers/article');
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('Go5ep02iVDQ7pwkHHaVUiw');
var Handlebars = require('handlebars');

var articleModel = new Article();
var categoryModel = new Category();
var articleHelper = new ArticleHelper();

var FIRST_PAGE_ARTICLE_COUNT = 60;
var NEXT_PAGE_ARTICLE_COUNT = 30;

var appContext = undefined;

var categoriesList = undefined;
var currentArticle = undefined;
var articlesList = undefined;
var topViewDayArticles = undefined;
var topViewWeekArticles = undefined;

var lastCacheCategory = undefined;
var lastCacheArticle = undefined;

exports.registerRoutes = function(app) {
  appContext = app;
  // Main page
  app.get('/', getAllCategories, getAllArticles, function(req, res) {
    var firstPageArticlesList = [];
    if (articlesList.length <= FIRST_PAGE_ARTICLE_COUNT) {
      firstPageArticlesList = articlesList;
    } else {
      for (var i = 0; i < FIRST_PAGE_ARTICLE_COUNT; i++) {
        firstPageArticlesList.push(articlesList[i]);
      }
    }
    res.render('index', {
      categories : categoriesList,
      articles : firstPageArticlesList,
      currentPage: firstPageArticlesList.length >= FIRST_PAGE_ARTICLE_COUNT ? 0 : -1,
      metaTitle : "Ai Báo Đây!",
      metaDescription : "Báo đây, ai báo đây. Tin nóng, tin nhanh, hotgirl, showbiz, chính trị, quốc tế... Nóng hổi vừa thổi vừa coi!",
      metaImage : "http://aibaoday.com/images/logo_aibaoday.png",
      metaUrl : "http://aibaoday.com",
      helpers : {
        loadFirstPageArticleList: function(dataList) {
          return new Handlebars.SafeString(articleHelper.generateListTags(dataList));
        }
      }
    });
  });

  // View articles of the next page
  app.get('/view/:page', getAllCategories, getAllArticles, function(req, res) {
    var currentPage = parseInt(req.params.page);
    var startIndex = FIRST_PAGE_ARTICLE_COUNT + (currentPage * NEXT_PAGE_ARTICLE_COUNT);
    if (startIndex > articlesList.length) startIndex = articlesList.length;
    var endIndex = startIndex + NEXT_PAGE_ARTICLE_COUNT;
    if (endIndex > articlesList.length) endIndex = articlesList.length;
    var nextPageArticlesList = [];
    for (var i = startIndex; i < endIndex; i++) {
      nextPageArticlesList.push(articlesList[i]);
    }
    if (nextPageArticlesList.length > 0) {
      res.send({ success: true, articles: nextPageArticlesList });
    } else {
      res.send({ success: false });
    }
  });

  // View by category page
  app.get('/top/:category', getAllCategories, getAllArticles, function(req, res) {
    viewByCategory(req, res);
  });

  // View articles of the next category page
  app.get('/top/:category/:page', getAllCategories, getAllArticles, function(req, res) {
    var categoryNameFormat = req.params.category;
    var currentCategory = undefined;
    for (var i = 0; i < categoriesList.length; i++) {
      if (categoriesList[i].nameFormat === categoryNameFormat) {
        currentCategory = categoriesList[i];
      }
    }
    if (currentCategory) {
      var currentCategoryName = currentCategory.name;
      var currentCategoryPage = parseInt(req.params.page);
      var startIndex = FIRST_PAGE_ARTICLE_COUNT + (currentCategoryPage * NEXT_PAGE_ARTICLE_COUNT);
      if (startIndex > articlesList.length) startIndex = articlesList.length;
      var nextPageCategoryArticlesList = [];
      var categoryArticleIndex = 0;
      for (var i = 0; i < articlesList.length; i++) {
        if (articlesList[i].category === currentCategoryName) {
          if (categoryArticleIndex >= startIndex) {
            nextPageCategoryArticlesList.push(articlesList[i]);
          } else {
            categoryArticleIndex = categoryArticleIndex + 1;
          }
        }
        if (nextPageCategoryArticlesList.length >= NEXT_PAGE_ARTICLE_COUNT) break;
      }
      if (nextPageCategoryArticlesList.length > 0) {
        res.send({ success: true, articles: nextPageCategoryArticlesList });
      } else {
        res.send({ success: false });
      }
    } else {
      res.send({ success: false });
    }
  });

  // Rewrite URL for article detail view page
  // app.get('/article/:articleId/:articleName', getAllCategories, getArticleDetail, function(req, res) {
  //   var currentArticleName = req.params.articleName;
  //   res.redirect('/' + formatTextForUrl(currentArticleName));
  // });

  // View article details
  app.get('/:articleName', getAllCategories, getArticleDetail, function(req, res) {
    // Get 5 latest articles
    articleModel.getLatest(currentArticle.id, 5, function (latestError, latestDocs) {
      if (latestError) {
        console.log("Something wrong in database: latest articles");
        res.status(500);
        res.render('errors/500');
      } else {
        var latestArticles = [];
        var latestIds = [];
        for (var i = 0; i < latestDocs.length; i++) {
          latestArticles.push(formatArticle(latestDocs[i]));
          latestIds.push(latestDocs[i]._id);
        }
        latestIds.push(currentArticle.id);
        // Get 5 top view today
        articleModel.getTopViewToday(latestIds, 10, function (topError, topDocs) {
          if (topError) {
            console.log("Something wrong in database: top view articles - " + topError);
            res.status(500);
            res.render('errors/500');
          } else {
            var dailyTopArticles = [];
            var weeklyTopArticles = [];
            for (var i = 0; i < topDocs.length; i++) {
              if (dailyTopArticles.length < 5) {
                dailyTopArticles.push(formatArticle(topDocs[i]));
              } else {
                weeklyTopArticles.push(formatArticle(topDocs[i]));
              }
            }
            res.render('articles/detail', {
              categories : categoriesList,
              article : currentArticle,
              metaTitle : currentArticle.title,
              metaDescription : currentArticle.description,
              metaImage : currentArticle.thumbnailImage,
              metaUrl : "http://aibaoday.com/" + currentArticle.titleUrl,
              latestArticles: latestArticles,
              dailyTopArticles : dailyTopArticles,
              weeklyTopArticles : weeklyTopArticles
            });
          }
        });
      }
    });
  });

  // Contact view
  app.get('/contact/form', function(req, res) {
    res.render('contact');
  });

  // Contact send message
  app.post('/contact/send', function(req, res) {
    var contactNickname = req.body.nickname;
    var contactEmail = req.body.email;
    var contactSubject = req.body.subject;
    var contactMessage = req.body.message;
    if (!contactNickname || !contactEmail || !contactSubject || !contactMessage) {
      return res.send({ success: false });
    }
    var mailParams = {
      "message" : {
        "from_email" : contactEmail,
        "from_name" : contactNickname,
        "to":[{"email" : "contact.aibaoday@gmail.com"}],
        "subject": contactSubject,
        "text": contactMessage
      },
      "async" : false
    };
    mandrill_client.messages.send(
      mailParams,
      function(result) {
        console.log(result);
        return res.send({ success: true });
      },
      function(error) {
        console.log('A mandrill error occurred: ' + error.name + ' - ' + error.message);
        return res.send({ success: false });
      });
  });

  app.get('/redirect/index.php', function(req, res) {
    var urlPath = req.query.link;
    res.redirect(urlPath);
  })

  /******* Mobile API - START *******/

  // Get all articles for main screen with page number
  app.get('/api/article/all/:page', getAllArticles, function(req, res) {
    var currentPage = req.params.page;
    var maxPage = Math.ceil(articlesList / NEXT_PAGE_ARTICLE_COUNT);
    if (currentPage > maxPage) {
      currentPage = maxPage;
    }
    var currentPageArticles = [];
    if (articlesList.length <= NEXT_PAGE_ARTICLE_COUNT * currentPage) {
      currentPageArticles = articlesList;
    } else {
      var fromIndex = (currentPage - 1) * NEXT_PAGE_ARTICLE_COUNT;
      var toIndex = fromIndex + NEXT_PAGE_ARTICLE_COUNT;
      for (var i = fromIndex; i < toIndex; i++) {
        if (articlesList[i]) {
          currentPageArticles.push(articlesList[i]);
        }
      }
    }
    res.json({ articles : currentPageArticles });
  });

  // Get all categories for Category screen
  app.get('/api/category/all', getAllCategories, function(req, res) {
    res.json({ categories : categoriesList });
  });

  // Get all articles filter by category and page number
  app.get('/api/article/:category/:page', getAllArticles, function(req, res) {
    res.json(null);
  });

  /******* Mobile API - END *******/
};

// Category
// Get all category items
function getAllCategories(req, res, next) {
  if (cachedCategories()) {
    next();
    return;
  }
  categoryModel.getAllByViewOrder(function(err, docs) {
    if (err) {
      categoriesList = [];
    } else {
      lastCacheCategory = getCurrentDateTime();
      categoriesList = docs;
    }
    next();
  });
}

// Get all Articles
function getAllArticles(req, res, next) {
  if (cachedArticles()) {
    next();
    return;
  }
  articleModel.getActive(function(articleError, articles) {
    if (articleError) {
      console.log("Something wrong in database!");
      res.status(500);
      res.render('errors/500');
    } else {
      lastCacheArticle = getCurrentDateTime();
      articlesList = [];
      for (var i = 0; i < articles.length; i++) {
        articlesList.push(formatArticle(articles[i]));
      }
      next();
    }
  });
}

// Get all articles by category
function viewByCategory(req, res) {
  var categoryNameFormat = req.params.category;
  var currentCategory = undefined;
  for (var i = 0; i < categoriesList.length; i++) {
    if (categoriesList[i].nameFormat === categoryNameFormat) {
      currentCategory = categoriesList[i];
    }
  }
  if (currentCategory) {
    var categoryName = currentCategory.name;
    var articlesByCategory = [];
    for (var i = 0; i < articlesList.length; i++) {
      if (articlesList[i].category === categoryName) {
        articlesByCategory.push(articlesList[i]);
      }
      if (articlesByCategory.length >= FIRST_PAGE_ARTICLE_COUNT) break;
    }
    res.render('index', {
      categories : categoriesList,
      category : currentCategory,
      articles : articlesByCategory,
      currentPage: articlesByCategory.length >= FIRST_PAGE_ARTICLE_COUNT ? 0 : -1,
      metaTitle : "Ai Báo Đây!",
      metaDescription : "Báo đây, ai báo đây. Tin nóng, tin nhanh, hotgirl, showbiz, chính trị, quốc tế... Nóng hổi vừa thổi vừa coi!",
      metaImage : "http://aibaoday.com/images/logo_aibaoday.png",
      metaUrl : "http://aibaoday.com",
      helpers : {
        loadFirstPageArticleList: function(dataList) {
          return new Handlebars.SafeString(articleHelper.generateListTags(dataList));
        }
      }
    });
  } else {
    console.log("Cannot find Category with nameFormat: " + req.params.category);
    res.status(500);
    res.render('errors/500');
  }
}

// Get Article detail
function getArticleDetail(req, res, next) {
  var titleUrl = req.params.articleName;
  if (titleUrl === "admin") {
    res.redirect('/admin/index');
    return;
  }
  articleModel.getByTitleUrl(titleUrl, function(articleError, articleDoc) {
    if (articleError || !articleDoc || articleDoc.length <= 0) {
      console.log("Cannot find Article with name: " + req.params.articleName);
      res.redirect('/');
    } else {
      articleDoc[0].views = articleDoc[0].views + 1;
      articleDoc[0].save(function(saveError) {
        if (saveError) {
          console.log("Cannot update views count of Article: " + articleDoc[0]._id);
        }
      });
      currentArticle = formatArticle(articleDoc[0]);
      next();
    }
  });
}

// Format the article information for UI view
function formatArticle(inputArticle) {
  return {
    id: inputArticle._id,
    title : inputArticle.title,
    titleUrl : inputArticle.titleUrl,
    description : inputArticle.description,
    category : inputArticle.category,
    categoryUrl : inputArticle.formatTextForUrl(inputArticle.category),
    thumbnailImage : inputArticle.thumbnailImage,
    author : inputArticle.author,
    content : inputArticle.content,
    tags : inputArticle.tags,
    views : inputArticle.views,
    updatedAt : formatDateTime(inputArticle.updatedAt), // for UI view
  };
}

function formatDateTime(datetime) {
  var hours = datetime.getHours();
  var minutes = datetime.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return datetime.getDate() + "/" + parseInt(datetime.getMonth()+1) + "/" + datetime.getFullYear() + "  " + strTime;
}

// Check categories list was cached or not
function cachedCategories() {
  if (!lastCacheCategory || !appContext.locals.categoryLastUpdatedAt) {
    return false;
  }
  return (lastCacheCategory.getTime() >= appContext.locals.categoryLastUpdatedAt.getTime()) && (categoriesList !== "null") && (categoriesList !== "undefined") && (parseInt(categoriesList.length) > 0);
}

// Check articles list was cached or not
function cachedArticles() {
  if (!lastCacheArticle || !appContext.locals.articleLastUpdatedAt) {
    return false;
  }
  return (lastCacheArticle.getTime() >= appContext.locals.articleLastUpdatedAt.getTime()) && (articlesList !== "null") && (articlesList !== "undefined") && (parseInt(articlesList.length) > 0);
}

function getCurrentDateTime() {
	var currentUTCTime = new Date();
	currentUTCTime.setMinutes(currentUTCTime.getMinutes() - 120); // convert to Vietnam local time
	return currentUTCTime;
}
