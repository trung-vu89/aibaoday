exports.registerRoutes = function(app, passport) {
  // Show Sign In view page
  app.get('/users/sign_in', function(req, res) {
    if (req.isAuthenticated()) {
      res.redirect('/admin'); // TODO: in this phase, just admin/moderators/content managers can log into the system.
      return;
    }
    res.render('users/sign_in', { layout: 'user', csrf: 'users_sign_in', message: req.flash('message') });
  });

  // Show Sign Up view page
  app.get('/users/sign_up', function(req, res) {
    if (req.isAuthenticated())
      req.logout();
    res.render('users/sign_up', { layout: 'user', csrf: 'users_sign_up', message: req.flash('message') });
  });

  // Handle Sign In request
  app.post('/users/sign_in',
    passport.authenticate('sign_in', {
      successRedirect: '/admin',
      failureRedirect: '/users/sign_in',
      failureFlash : true
    })
  );

  // Handle Sign Up request
  app.post('/users/sign_up', passport.authenticate('sign_up', {
    successRedirect: '/admin',
		failureRedirect: '/users/sign_up',
		failureFlash : true
  }));

  // Handle Sign Out request
  app.post('/users/sign_out', function(req, res) {
		req.logout();
		if (req.xhr || req.accepts('json, html') === 'json'){
			// if there were an error, we would send { error: 'error description' }
			res.send({ success: true });
		} else {
			// if there were an error, we would redirect to an error page
			res.redirect(303, '/users/sign_in');
		}
	});
};
