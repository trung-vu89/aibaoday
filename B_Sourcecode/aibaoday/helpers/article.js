var Article = function() {
  // constructor
}

Article.prototype.generateListTags = function(articlesList) {
  var output = "";
  for (var i = 0; i < articlesList.length; i++) {
    var articleData = articlesList[i];
    var newArticle =
    '<article class="post_type_1">' +
      '<div class="feature"> ' +
        '<div class="image"> ' +
          '<a href="/' + articleData.titleUrl + '" style="width: 300px; height: 180px;">' +
            '<img src="' + articleData.thumbnailImage + '" style="max-width: 100%; max-height: 100%; margin: auto; display: block;"><span class="hover"></span>' +
          '</a>' +
        '</div>' +
      '</div>' +
      '<div class="content">' +
        '<div class="info">' +
          '<div class="tags"><a href="/top/' + articleData.categoryUrl + '">' + articleData.category + '</a></div>' +
          '<div class="date"> - ' + articleData.updatedAt + '</div>' +
          '<div class="stats">' +
            '<div class="likes">' + articleData.views + '</div>' +
          '</div>' +
        '</div>' +
        '<div class="title" style="height: 41px;">' +
          '<a href="/' + articleData.titleUrl + '">' + articleData.title + '</a>' +
        '</div>' +
      '</div>' +
    '</article>';
    output = output + newArticle;
  }
  return output;
}

module.exports = Article;
